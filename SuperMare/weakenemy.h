#ifndef WEAKENEMY_H
#define WEAKENEMY_H

#include "enemy.h"
#include<QGraphicsScene>

class WeakEnemy: public Enemy
{
public:
    WeakEnemy(qreal posX, qreal posY, qreal width, qreal height, QString enemyImg, QGraphicsScene * scene);

    void harmPlayer(player *p) override;

    void detectCollision();

    void advance(int step) override;

    ~WeakEnemy();
private:
    qreal _points = 10;
};

#endif // WEAKENEMY_H
