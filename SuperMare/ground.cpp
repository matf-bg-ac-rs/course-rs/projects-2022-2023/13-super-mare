#include "ground.h"

Ground::Ground(qreal posX, qreal posY, qreal width, QString groundImg)
    :_groundImg(groundImg)
{
    qreal height = width / 3.0;
    setPixmap(QPixmap(_groundImg).scaled(width, height));
    setPos(posX, posY);
}
Ground::~Ground() {

//TODO

}
