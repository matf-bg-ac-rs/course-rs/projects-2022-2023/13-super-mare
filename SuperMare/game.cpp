#include "game.h"
#include "deadlyenemy.h"
#include "key.h"
#include "player.h"
#include "background.h"
#include "playerposition.h"
#include "coin.h"
#include "life.h"
#include "weakenemy.h"
#include "ranglist.h"
#include "gameOverOptions.h"

//#include "speed.h"

#include <map>
#include <QFile>
#include <QTimer>
#include <QMessageBox>

Game::Game(LevelNum selectedLevel, QString name, QWidget *parent)
    :name(name){
    scene = new QGraphicsScene(parent);
    scene->setSceneRect(-400, -400, 800, 500);

    //podesavanja putanja za igru
    if(selectedLevel == LevelNum::Level1) {
        mapPath = ":/map/mapa1.txt";
        backgroundImg = ":/images/backgroundLevel1";
        deImg = ":/images/mushroom.png";
        weImg = ":/images/enemy1.png";
        lifeImg = ":/images/life1";
        coinImg = ":/images/coin";
        groundImg = ":/images/ground";
        keyImg = ":/images/key.png";

    } else {
        mapPath = ":/map/mapa2.txt";
        backgroundImg = ":/images/backgroundLevel2";
        deImg = ":/images/mushroom.png";
        weImg = ":/images/enemy1.png";
        lifeImg = ":/images/life2";
        coinImg = ":/images/coin";
        groundImg = ":/images/ground2";
        keyImg = ":/images/key.png";

    }

    //dodavanje pozadine
    backgroundScreen = new Background(-400, -400, scene->width(), scene->height(), backgroundImg);
    
//TODO
    currPlayer = new player(backgroundScreen->qPixMapWidth(), scene->width());
    currPlayer->setFlag(QGraphicsItem::ItemIsFocusable);
    currPlayer->setFocus();

    currPlayer->playerScore->setPos(scene->sceneRect().topLeft());

    //kreiranje mape
    mapCreator(mapPath);

    // dodajemo itm-e na scenu    

    scene->addItem(backgroundScreen);
    scene->addItem(currPlayer);
    scene->addItem(currPlayer->playerScore);
    scene->addItem(currPlayer->playerHealth);
    foreach( QGraphicsItem *item, items ){
            scene->addItem(item);
    }


    // make the newly created scene the scene to visualize (since Game is a QGraphicsView Widget,
    // it can be used to visualize scenes
    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setWindowTitle("Super Mare");
    setFixedSize(800, 500);
    show();

    QTimer *timer = new QTimer(this);
    // Postavljamo da se advance poziva na svakih 1000/33 ms sto otprilike daje 30fps.
    timer->start(1000 / 33);

    QObject::connect(currPlayer, SIGNAL(positionChange(PlayerDirection, PlayerPosition)), this, SLOT(onPlayerPositionChange(PlayerDirection, PlayerPosition)));
    QObject::connect(currPlayer, SIGNAL(gameIsOver(GameOverOptions)), this, SLOT(onGameIsOver(GameOverOptions)));
    QObject::connect(this, SIGNAL(endOfMap()), currPlayer, SLOT(setFlagOfEnd()));
    QObject::connect(timer,SIGNAL(timeout()),scene,SLOT(advance()));
}

Game::~Game() {
    foreach( QGraphicsItem *item, items ){
        delete item;
    }

    delete scene;
    delete currPlayer;
    delete backgroundScreen;
}



void Game::addItems(char type, int number, qreal middleParameter)
{
    qreal deltaUpper = 40;
    for(int i = 0; i < number; i++){
        switch (type) {
        case 'G':
            items.append(new Ground(currentMapPath + currPlayer->x() + i * currPlayer->width(),
                                    currPlayer->y()  + currPlayer->height() - componentUpper,
                                    scene->width() * 0.1, groundImg));
            break;
        case 'C':
            items.append(new Coin(currentMapPath + currPlayer->x() + i * currPlayer->width() + middleParameter,
                                  currPlayer->y()  + currPlayer->height() - deltaUpper - componentUpper,
                                  scene->width() * 0.05, scene->height() *0.05, coinImg, scene));
            break;
        case 'L':
            items.append(new Life(currentMapPath + currPlayer->x() + i * currPlayer->width() + middleParameter,
                                  currPlayer->y()  + currPlayer->height() - deltaUpper - componentUpper,
                                  scene->width() * 0.05, scene->height() *0.05,lifeImg, scene));
            break;
        case 'U':
            componentUpper += number * 10;
            break;
        case 'W':
            items.append(new WeakEnemy(currentMapPath + currPlayer->x() + i * currPlayer->width() + middleParameter,
                                  currPlayer->y()  + currPlayer->height() - deltaUpper - componentUpper,
                                  scene->width() * 0.05, scene->height() *0.05, weImg, scene));
            break;
        case 'D':
            items.append(new DeadlyEnemy(currentMapPath + currPlayer->x() + i * currPlayer->width() + middleParameter,
                                  currPlayer->y()  + currPlayer->height() - deltaUpper - componentUpper,
                                  scene->width() * 0.05, scene->height() *0.05, deImg, scene));
            break;
        case 'K':
            items.append(new Key(currentMapPath + currPlayer->x() + i * currPlayer->width() + middleParameter,
                                  currPlayer->y()  + currPlayer->height() - deltaUpper - componentUpper,
                                  scene->width() * 0.1, scene->height() *0.1, keyImg));
            break;
        default:
            break;
        }
    }    
}

void Game::mapCreator(QString filePath)
{
    QFile file(filePath);
    if(!file.exists()){
        qDebug() << "File not exist";
        return;
    }
    if(!file.open(QIODevice::ReadOnly)){
        qDebug() << "Opening failed";
        return;
    }


    QTextStream in(&file);
    QStringList line = in.readLine().split(" ");
    qreal numOfLine = line[0].toInt();
    qreal numberOfGround = 0;
    qreal deltaDistnce = 50;

    for(int i = 0; i < numOfLine; i++){
        QString objects = in.readLine();
        //poslednji objekat je samo flag za distance
        QString withDistance = objects[objects.size()-1];

        qreal middleParameter = objects[3].digitValue() / 2.6 * currPlayer->width();
        for(int j = 0; j < objects.size() - 2; j += 2){
           char type = objects[j].toLatin1();
           int number = objects[j + 1].digitValue();
           addItems(type, number, middleParameter);
        }
        numberOfGround = objects[3].digitValue();
        if(withDistance == "Y") {
            currentMapPath += (numberOfGround) * currPlayer->width() + deltaDistnce;
        } else {
            currentMapPath += (numberOfGround) * currPlayer->width();
        }
        componentUpper = 0;
    }

    file.close();
}



void Game::moveBackground(qreal deltaX) const
{
    backgroundScreen->moveBy((-1.0) * deltaX,0);
    foreach( QGraphicsItem *item, items ){
        item->moveBy((-1.0) * deltaX,0);
    }
}

void Game::onPlayerPositionChange(PlayerDirection direction, PlayerPosition playerPosition)
{
    if(playerPosition == PlayerPosition::MIDDLE && direction == PlayerDirection::FORWARD){
        if(backgroundScreen->qPixMapWidth() + backgroundScreen->x() - scene->width() / 2 <= 5){
            emit endOfMap();
        }

        if(!currPlayer->isOnGround())
            moveBackground(90);
        moveBackground(10);
    }
    else if(playerPosition == PlayerPosition::MIDDLE && direction == PlayerDirection::BACK){
        if(backgroundScreen->qPixMapWidth() + backgroundScreen->x() - scene->width() / 2 <= 5){
            emit endOfMap();
        }

        if(!currPlayer->isOnGround())
            moveBackground(-90);
        moveBackground(-10);
    }

}

void Game::onGameIsOver(GameOverOptions option) {
    QObject::disconnect(currPlayer, SIGNAL(gameIsOver(GameOverOptions)), this, SLOT(onGameIsOver(GameOverOptions)));
    QMessageBox msgBox;
    this->close();
    msgBox.setStyleSheet("QLabel{min-width:350 px; font-size: 18px;} QPushButton{ width:150px; font-size: 18px;}");
    msgBox.setText("Game is over.");
    if(option == GameOverOptions::Lost)
        msgBox.setInformativeText("Sorry. You lost.");
    else {
        msgBox.setInformativeText("You won this game!\nYour score is: " + QString::number(currPlayer->playerScore->getScore()));
        std::pair<QString, int> player = std::make_pair(name, currPlayer->playerScore->getScore());

        QString currPath = QDir::currentPath().mid(0, QDir::currentPath().lastIndexOf('/'));
        QFile players(currPath + "/SuperMare/players.txt");

        players.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);

        QTextStream stream(&players);
        RangList *ra = new RangList;
        ra->addPlayer(player.first, player.second);

        players.close();
        delete ra;
    }
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.exec();
}
