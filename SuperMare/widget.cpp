#include "widget.h"
#include "LevelNum.h"
#include "game.h"
#include "ui_widget.h"
#include "ranglist.h"
#include <QMessageBox>
#include<iostream>

Game *game;

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
    , ranglist (new RangList)
{
    ui->setupUi(this);


}

Widget::~Widget()
{
    delete ui;
}




void Widget::on_PlayButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void Widget::on_pushButton_3_clicked()
{
    LevelNum selectedLevel = LevelNum::Level1;
    if((!ui->rBlevel1->isChecked() && !ui->rBlevel2->isChecked())or (ui->TextName->toPlainText() == "")
        ) {
        QMessageBox msgBox;
        msgBox.setStyleSheet("QLabel{min-width:350 px; font-size: 18px;} QPushButton{ width:150px; font-size: 18px;}");
        msgBox.setText("Please select level and enter name!");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.exec();
        return;
    }
    if(ui->rBlevel2->isChecked()){
        selectedLevel = LevelNum::Level2;
    } else {
        selectedLevel = LevelNum::Level1;
    }

    QString name = ui->TextName->toPlainText();
    ui->TextName->clear();

    game = new Game(selectedLevel, name, this);
    game->show();
    ui->pushButton_3->setText("Play again?");
}


void Widget::on_RangList_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
    ui->labelPlayers->setText(ranglist->printListToRangList());
}


void Widget::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}


void Widget::on_pushButton_2_clicked()
{
     ui->stackedWidget->setCurrentIndex(3);
}


void Widget::on_pushButton_4_clicked()
{
     ui->stackedWidget->setCurrentIndex(0);
}


void Widget::on_Exit_clicked()
{
    this->close();
}


void Widget::on_pushButton_5_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

