#include "background.h"

#include <QPainter>

Background::Background(qreal positionX, qreal positionY, qreal width, qreal height, QString backgroundImg)
    : _backgroundImg(backgroundImg)
{
    _qPixMap = QPixmap(_backgroundImg).scaled(width, height , Qt::AspectRatioMode::KeepAspectRatioByExpanding);
    setPixmap(_qPixMap);
    setPos(positionX, positionY);
}

Background::~Background()
{
}

int Background::qPixMapWidth() const
{
    return _qPixMap.width();
}


