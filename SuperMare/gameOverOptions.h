#ifndef GAMEOVEROPTIONS_H
#define GAMEOVEROPTIONS_H

enum class GameOverOptions {
    Lost,
    Won
};

#endif // GAMEOVEROPTIONS_H
