#include "health.h"
#include "qfont.h"

Health::Health()
    :_lifes(3) {
    setText();
    setDefaultTextColor(Qt::red);
    setFont(QFont("times",16));
}

void Health::setText() {
    QString toShow = "";
    for(int i=0; i<_lifes; i++){
        toShow += "❤️";
    }
    setPlainText(toShow);
}
void Health::addLife() {
    if(_lifes < 3) {
        _lifes ++;
    }
    setText();
}
void Health::removeLife() {
    _lifes --;
    setText();
}

int Health::getLifes() {
    return _lifes;
}

Health::~Health() {
}
