#ifndef DEADLYENEMY_H
#define DEADLYENEMY_H

#include "enemy.h"

#include<QDebug>
#include<QGraphicsScene>

class DeadlyEnemy : public Enemy {
public:
    DeadlyEnemy(qreal posX, qreal posY, qreal width, qreal height, QString enemyImg, QGraphicsScene * _scene);

    void harmPlayer(player *p) override;

    void detectCollision();

    void advance(int step) override;

    ~DeadlyEnemy();

};

#endif // DEADLYENEMY_H
