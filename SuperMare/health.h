#ifndef HEALTH_H
#define HEALTH_H

#include <QGraphicsTextItem>

class Health: public QGraphicsTextItem {
public:
    Health();
    void addLife();
    void removeLife();
    int getLifes();
    ~Health();

private:
    int _lifes;
    void setText();
};

#endif // HEALTH_H
