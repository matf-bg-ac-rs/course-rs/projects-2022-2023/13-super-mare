#include "player.h"
#include "key.h"
#include "qapplication.h"

#include "enemy.h"
#include "friend.h"


player::player(qreal mapSize, qreal screenWidth) : _mapSize(mapSize), _screenWidth(screenWidth)
{
    soundManager = new SoundManager(":/sounds/backgroundMusic.wav",
                                    ":/sounds/collect.wav",
                                    ":/sounds/error.wav");
    soundManager->playMusic();
    playerHealth = new Health();
    playerScore = new Score();
    deltaS = 0;
    _scrnHeight = 815;
    _height = _scrnHeight / 6.0;
    _width = _height * 0.5;
    _stepY = 150;
    _gravity = _stepY * 0.1;

    setPixmap(QPixmap(_currImg).scaled(_width,_height));
    setPos(_posX, _posY);

}
qreal player::width() const{
    return _width;
}

bool player::isOnGround() const
{
    return _isOnGround;
}

qreal player::X() const
{
    return _X;
}

void player::setFlagOfEnd()
{
    flagOfEnd = true;
}

qreal player::height() const {
    return _height;
}


player::~player() {
    delete soundManager;
}

bool player::isDead() {
    return y() > 400 || playerHealth->getLifes() == 0;
}

void player::walk() {
    if(_X == 0) {
         return;
     }
    if((_currentDistance - _screenWidth / 2) <= _epsilonMiddle){
         setPos(x() + _X, y());
         _currentPlayerPosition = PlayerPosition::START;
         _currentDistance += _X;
    }
    else if(flagOfEnd){
        setPos(x() + _X, y());
        _currentPlayerPosition = PlayerPosition::END;
        _currentDistance += _X;
    }
    else{
         _currentPlayerPosition = PlayerPosition::MIDDLE;
         _currentDistance += _X;
    }
    if(_currImg == _imgPlayer1 and _steps == 8) {
         _currImg = _imgPlayer2;
         _steps = 0;
     }else if(_currImg == _imgPlayer2 and _steps == 8){
         _currImg = _imgPlayer1;
         _steps = 0;
     }
     _steps++;
}

void player::jump() {
    if(!_isOnGround) {
        setPos(x(), y() + _gravity);
        _Y += _gravity;
    }
}


void player::getScreenHeight(){
    QScreen *screen = QApplication::screens().at(0);
    _scrnHeight = screen->availableSize().height();
}

void player::getScreenWidth(){
    QScreen *screen = QApplication::screens().at(0);
    _screenWidth = screen->availableSize().width();
}

void player::advance(int step) {
    if(!step)
        return;
    if(isDead()) {
        soundManager->stopMusic();
        emit gameIsOver(GameOverOptions::Lost);
    }
    walk();
    jump();
    detectCollision();
    setPixmap(QPixmap(_currImg).scaled(_width, _height));

    playerHealth->setPos(x() - width()*0.1, y() - height()*0.15);
}

void player::keyPressEvent(QKeyEvent *event){
    if(event->key() == Qt::Key_Escape){
        _X = 0;
        _Y = 0;
    }
    if(event->key() == Qt::Key_Right){
        _currentPlayerDirection = PlayerDirection::FORWARD;
        if(_currImg == _imgPlayer) {
            _currImg = _imgPlayer1;
        }
        if(_isOnGround)
            _X = _stepX;
        else
            _X = 2.5*_stepX;
        emit positionChange(_currentPlayerDirection, _currentPlayerPosition);
    }
    if(event->key() == Qt::Key_Left){
        _currentPlayerDirection = PlayerDirection::BACK;
        if(_currImg == _imgPlayer) {
            _currImg = _imgPlayer1;
        }
        if(_isOnGround)
            _X = (-1) *_stepX;
        else
            _X = (-2.5)*_stepX;
        emit positionChange(_currentPlayerDirection,  _currentPlayerPosition);
    }
    if(event->key() == Qt::Key_Up and _isOnGround){
        _Y -= _stepY;
        _isOnGround = false;
        setPos(x(), y() + _Y);
    }
}

void player::keyReleaseEvent(QKeyEvent *event){
    if(event->key() == Qt::Key_Right or event->key() == Qt::Key_Left){
        _X = 0;
    }
    else {
        event->ignore();
    }
}

void player::detectCollision() {

    QList<QGraphicsItem *> colliding_items = collidingItems();

    bool isOnGround_tmp = false;

    if(colliding_items.size()) {
        for (auto &colliding_item : colliding_items) {
            if(dynamic_cast<Ground*>(colliding_item)) {
                this->stayOnGround(colliding_item);
                isOnGround_tmp = true;
            }
            if(dynamic_cast<Key*>(colliding_item)) {
                emit gameIsOver(GameOverOptions::Won);
            }
        }
    }
    if(!isOnGround_tmp) {
        _isOnGround = false;
    }
}

void player::stayOnGround(QGraphicsItem *platform){

    QRectF platformRect          = platform->boundingRect();
    QPolygonF platformRectPoints = platform->mapToScene(platformRect);

    QPolygonF _playerRectPoints = mapToScene(boundingRect());

    qreal delta = 20;
    if(_playerRectPoints[2].y() <= platformRectPoints[0].y() + delta) {
       _isOnGround = true;
    }
    else if(!_isOnGround && _playerRectPoints[3].x() <= platformRectPoints[3].x() - delta &&
            _playerRectPoints[1].y() <= platformRectPoints[3].y() - delta){
        setPos(x() - 10, y());
    } else if(!_isOnGround && _playerRectPoints[2].x() >= platformRectPoints[2].x() + delta &&
            _playerRectPoints[1].y() <= platformRectPoints[3].y() - delta)
    {
        setPos(x() + 10, y());
    }
}
