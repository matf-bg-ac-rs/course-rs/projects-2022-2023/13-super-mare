#ifndef LIFE_H
#define LIFE_H

#include "friend.h"

#include<QDebug>

class Life : public Friend
{
public:
    Life(qreal posX, qreal posY, qreal width, qreal height, QString friendImg,  QGraphicsScene *scene);

    void helpPlayer(player *p) override;

    void detectCollision();

    void advance(int step) override;

    ~Life();
};

#endif // LIFE_H
