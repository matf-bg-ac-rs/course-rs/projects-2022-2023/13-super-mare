#ifndef FRIEND_H
#define FRIEND_H

#include "qglobal.h"
#include "qgraphicsitem.h"
#include "qobject.h"

#include <QGraphicsPixmapItem>

#include "player.h"

class Friend: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Friend(qreal posX, qreal posY, qreal width, qreal height, QString _imgFriend, QGraphicsScene *scene);

    ~Friend();
 protected:
    virtual void helpPlayer(player *p) = 0;
    qreal _posX;
    qreal _posY;

    qreal _height;
    qreal _width;
    QGraphicsScene * _scene;

    QString _imgFriend;

};

#endif // FRIEND_H
