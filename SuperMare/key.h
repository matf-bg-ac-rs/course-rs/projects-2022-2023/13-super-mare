#ifndef KEY_H
#define KEY_H
#include "qgraphicsitem.h"
#include "qobject.h"


class Key: public QObject, public QGraphicsPixmapItem
{
public:
    Key(qreal posX, qreal posY, qreal width, qreal height, QString _imgFriend);
    ~Key();
};

#endif // KEY_H
