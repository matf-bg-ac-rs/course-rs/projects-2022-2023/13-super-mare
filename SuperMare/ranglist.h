#ifndef RANGLIST_H
#define RANGLIST_H

#include <QWidget>
#include <QFile>
#include <QTextStream>

class RangList : public QWidget
{
public:
    RangList();
    QList<std::pair<QString, int>> playerList;
    void addPlayer(QString& name, int score);
    void readFromFileAndInsertIntoList();
    void insertPlayerIntoList(QString name, int score);
    QString printListToRangList();
    void printPlayersIntoFile();
    void sortPlayersByScore();
    ~RangList();
};

#endif // RANGLIST_H
