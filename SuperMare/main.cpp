#include "game.h"
#include "widget.h"

#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QTimer>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Widget w;

    a.setWindowIcon(QIcon(":/images/icon2.png"));
//TODO
    QSplashScreen *splash = new QSplashScreen;
    splash->setPixmap(QPixmap(":/images/splash.png"));
    splash->show();

    QTimer::singleShot(5000,splash,SLOT(close()));
    QTimer::singleShot(5000,&w,SLOT(show()));

    return a.exec();
}
