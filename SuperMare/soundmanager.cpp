#include "soundmanager.h"

SoundManager::SoundManager(QString musicPath, QString friendCollPath, QString enemyCollPath)
    : _musicPath(musicPath), _friendCollPath(friendCollPath), _enemyCollPath(enemyCollPath)
{
    backgroundPlayer = new QSoundEffect();
    soundPlayer = new QSoundEffect();
}

void SoundManager::playFriendSound() {
    soundPlayer->setSource(QUrl::fromLocalFile(_friendCollPath));
    soundPlayer->setVolume(60);
    soundPlayer->play();
}

void SoundManager::playEnemySound() {
    soundPlayer->setSource(QUrl::fromLocalFile(_enemyCollPath));
    soundPlayer->setVolume(60);
    soundPlayer->play();
}

void SoundManager::playMusic() {
    backgroundPlayer->setSource(QUrl::fromLocalFile(_musicPath));
    backgroundPlayer->setVolume(40);
    backgroundPlayer->setLoopCount(QSoundEffect::Infinite);
    backgroundPlayer->play();
}
void SoundManager::stopMusic() {
    backgroundPlayer->stop();
}
SoundManager::~SoundManager() {
    delete backgroundPlayer;
    delete soundPlayer;
}
