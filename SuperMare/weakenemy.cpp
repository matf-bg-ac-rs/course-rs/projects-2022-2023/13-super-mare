#include "weakenemy.h"

WeakEnemy::WeakEnemy(qreal posX, qreal posY, qreal width, qreal height, QString enemyImg, QGraphicsScene * scene):
    Enemy(posX, posY, width, height, enemyImg, scene) {
    setPixmap(QPixmap(_enemyImg).scaled(_width, _height));
    setPos(_posX, _posY);
}

void WeakEnemy::harmPlayer(player *p) {
    p->playerScore->removePoints(_points);
}


void WeakEnemy::detectCollision() {

    QList<QGraphicsItem *> colliding_items = collidingItems();

    if(colliding_items.size()) {
        for (auto &colliding_item : colliding_items ) {
            if(dynamic_cast<player*>(colliding_item)){
                harmPlayer(dynamic_cast<player*>(colliding_item));
                dynamic_cast<player*>(colliding_item)->soundManager->playEnemySound();
                this->_scene->removeItem(this);
            }
        }
    }
}
void WeakEnemy::advance(int step) {
    if(!step)
        return;
    detectCollision();
}

WeakEnemy::~WeakEnemy() {

}

