#ifndef SCORE_H
#define SCORE_H

#include <QGraphicsTextItem>

class Score: public QGraphicsTextItem
{
public:
    Score(QGraphicsItem *parent = 0);
    void addPoints(int points);
    void removePoints(int points);
    int getScore();
private:
    int _score;
    ~Score();
};

#endif // SCORE_H
