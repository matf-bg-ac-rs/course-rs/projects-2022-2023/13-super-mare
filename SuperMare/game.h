#ifndef GAME_H
#define GAME_H

#include "LevelNum.h"
#include "player.h"
#include "background.h"
#include "ranglist.h"

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QObject>

class Game:  public QGraphicsView
{
Q_OBJECT
public:
    Game(LevelNum selectedLevel, QString name, QWidget *parent);

    QGraphicsScene *scene;
    PlayerPosition currentPlayerPosition;
    player *currPlayer;
    Background *backgroundScreen;
    QList<QGraphicsItem *> items;
    qreal currentMapPath = 0;
    qreal componentUpper = 0;

    ~Game();

    void moveBackground(qreal deltaX) const;
    void addItems(char type, int number, qreal middleParameter);
    void mapCreator(QString filePath);
private:
    QString name;
    QString mapPath;
    QString backgroundImg;
    QString deImg;
    QString weImg;
    QString lifeImg;
    QString coinImg;
    QString groundImg;
    QString keyImg;
signals:
        bool endOfMap();

public slots:
    void onPlayerPositionChange(PlayerDirection direction, PlayerPosition playerPosition);
    void onGameIsOver(GameOverOptions option);
};

#endif // GAME_H
