#ifndef SPEED_H
#define SPEED_H

enum class Speed
{
    Fast = 3,
    Medium = 2,
    Slow = 1
};


#endif // SPEED_H
