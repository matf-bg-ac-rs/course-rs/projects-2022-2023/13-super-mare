#ifndef PLAYERPOSITION_H
#define PLAYERPOSITION_H


enum class PlayerPosition
{
    START,
    MIDDLE,
    END
};

enum class PlayerDirection
{
    FORWARD,
    BACK
};

#endif // PLAYERPOSITION_H
