QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Explicitly specify compilers
QMAKE_CXX = /usr/bin/g++
QMAKE_CC = /usr/bin/gcc

SOURCES += \
    background.cpp \
    coin.cpp \
    deadlyenemy.cpp \
    enemy.cpp \
    friend.cpp \
    game.cpp \
    ground.cpp \
    health.cpp \
    key.cpp \
    life.cpp \
    main.cpp \
    player.cpp \
    ranglist.cpp \
    score.cpp \
    soundmanager.cpp \
    weakenemy.cpp \
    widget.cpp

HEADERS += \
    LevelNum.h \
    background.h \
    coin.h \
    deadlyenemy.h \
    enemy.h \
    friend.h \
    game.h \
    gameOverOptions.h \
    ground.h \
    health.h \
    key.h \
    life.h \
    player.h \
    playerposition.h \
    ranglist.h \
    score.h \
    soundmanager.h \
    speed.h \
    weakenemy.h \
    widget.h

FORMS += \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    Resources.qrc

DISTFILES += \
    players.txt
