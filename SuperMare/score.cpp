#include "score.h"
#include "qfont.h"

Score::Score(QGraphicsItem *parent)
    :_score(0){

    setPlainText(QString("Score: ") + QString::number(_score)); // Score: 0
    setDefaultTextColor(Qt::white);
    setFont(QFont("times",16));
}
void Score::addPoints(int points) {
    _score += points;
    setPlainText(QString("Score: ") + QString::number(_score));
}
void Score::removePoints(int points) {
    _score -= points;
    setPlainText(QString("Score: ") + QString::number(_score));
}

int Score::getScore(){
    return _score;
}

Score::~Score() {
}
