#ifndef ENEMY_H
#define ENEMY_H

#include "player.h"
#include "qobject.h"

#include <QGraphicsPixmapItem>

class Enemy: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Enemy(qreal posX, qreal posY, qreal width, qreal height, QString _enemyImg, QGraphicsScene * scene);
    ~Enemy();
protected:
    virtual void harmPlayer(player *p) = 0;
    qreal _posX;
    qreal _posY;

    qreal _width;
    qreal _height;

    QGraphicsScene * _scene;

    QString _enemyImg;

};

#endif // ENEMY_H
