#include "coin.h"

Coin::Coin(qreal posX, qreal posY, qreal width, qreal height, QString imgFriend, QGraphicsScene *scene):
    Friend(posX, posY, width, height, imgFriend, scene) {
    setPixmap(QPixmap(_imgFriend).scaled(_width, _height));
    setPos(_posX, _posY);

}

void Coin::helpPlayer(player *p) {
    p->playerScore->addPoints(_points);
}

void Coin::detectCollision() {

    QList<QGraphicsItem *> colliding_items = collidingItems();

    if(colliding_items.size()) {
        for (auto &colliding_item : colliding_items ) {
            if(dynamic_cast<player*>(colliding_item)){
                helpPlayer(dynamic_cast<player*>(colliding_item));
                dynamic_cast<player*>(colliding_item)->soundManager->playFriendSound();
                this->_scene->removeItem(this);
            }
        }
    }
}

//dodat komentar


void Coin::advance(int step) {
    if(!step)
        return;
    detectCollision();
}

Coin::~Coin() {

}

