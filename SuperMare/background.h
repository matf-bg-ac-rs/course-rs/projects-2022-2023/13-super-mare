#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "qglobal.h"
#include "qgraphicsitem.h"
#include "qobject.h"
#include <speed.h>

#include <QGraphicsPixmapItem>


class Background: public QGraphicsPixmapItem, public QObject
{
public:

    Background(qreal positionX, qreal positionY, qreal width, qreal height, QString backgroundImg);
    ~Background();


    int qPixMapWidth() const;

private:
    QString _backgroundImg;
    QPixmap _qPixMap;

};

#endif // BACKGROUND_H
