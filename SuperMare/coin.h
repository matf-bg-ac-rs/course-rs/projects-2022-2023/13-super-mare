#ifndef COIN_H
#define COIN_H

#include "friend.h"
#include<QDebug>
#include<QGraphicsScene>

class Coin : public Friend
{
public:
    Coin(qreal posX, qreal posY, qreal width, qreal height, QString imgFriend,  QGraphicsScene *scene);

    void helpPlayer(player *p) override;

    void detectCollision();

    void advance(int step) override;

    ~Coin();
private:
    qreal _points = 3;
};

#endif // COIN_H
