#ifndef GROUND_H
#define GROUND_H

#include <QGraphicsPixmapItem>

class Ground: public QGraphicsPixmapItem, public QObject{
public:
    Ground(qreal posX, qreal posY, qreal width, QString groundImg);

    ~Ground();
private:
    QString _groundImg;
};

#endif // GROUND_H
