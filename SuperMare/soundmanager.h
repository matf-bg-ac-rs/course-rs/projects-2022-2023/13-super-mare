#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include <QtMultimedia>
#include <QSoundEffect>

class SoundManager
{
public:
    SoundManager(QString musicPath, QString friendCollPath, QString enemyCollPath);
    QSoundEffect *backgroundPlayer;
    QSoundEffect *soundPlayer;
    ~SoundManager();

    void playFriendSound();
    void playEnemySound();
    void playMusic();
    void stopMusic();

private:
    QString _musicPath;
    QString _friendCollPath;
    QString _enemyCollPath;

};

#endif // SOUNDMANAGER_H
