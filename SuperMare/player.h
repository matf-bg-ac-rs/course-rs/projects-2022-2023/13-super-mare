#ifndef PLAYER_H
#define PLAYER_H

#include "gameOverOptions.h"
#include "health.h"
#include "qobject.h"
#include "score.h"
#include "speed.h"
#include "ground.h"
#include "playerposition.h"
#include "soundmanager.h"

#include <QGraphicsPixmapItem>
#include <QScreen>
#include <QSplashScreen>
#include <QKeyEvent>
#include <QDebug>


class player: public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    public:
        player(qreal mapSize, qreal screenWidth);
        qreal deltaS;

        bool isDead();
        void jump();
        void walk();

        SoundManager *soundManager;
        Score *playerScore;
        Health *playerHealth;

        void keyPressEvent(QKeyEvent *event) override;
        void keyReleaseEvent(QKeyEvent *event) override;
        void detectCollision();
        void stayOnGround(QGraphicsItem *platform);

        void getScreenHeight();
        void getScreenWidth();

        virtual void advance(int step) override;
        ~player();

        qreal height() const;
        qreal width() const;

        bool isOnGround() const;

        qreal X() const;

signals:
        void positionChange(PlayerDirection direction, PlayerPosition playerPosition);
        void gameIsOver(GameOverOptions option);

public slots:
        void setFlagOfEnd();

private:
        QString _imgPlayer = ":/images/mare";
        QString _imgPlayer1  = ":/images/mare1";
        QString _imgPlayer2 = ":/images/mare2";

        qreal _X = 0;
        qreal _Y = 0;
        qreal _mapSize;
        bool flagOfEnd = false;

        PlayerPosition _currentPlayerPosition = PlayerPosition::START;
        PlayerDirection _currentPlayerDirection = PlayerDirection::FORWARD;

        qreal _screenWidth;
        qreal _scrnHeight;
        qreal _epsilonMiddle = 0.1;
        qreal _posX = -400;
        qreal _posY = -60;

        qreal _currentDistance = 0;
        qreal _stepX = 5;
        qreal _stepY = 30;
        qreal _gravity;
        bool _isOnGround = false;

        qreal _height;
        qreal _width;

        QString _currImg = _imgPlayer;
        int _steps = 0;    
};

#endif // PLAYER_H
