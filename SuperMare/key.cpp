#include "key.h"

Key::Key(qreal posX, qreal posY, qreal width, qreal height, QString _imgFriend) {
    setPixmap(QPixmap(_imgFriend).scaled(width, height));
    setPos(posX, posY);
}
Key::~Key() {}
