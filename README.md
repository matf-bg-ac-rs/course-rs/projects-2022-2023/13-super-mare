# <img src="SuperMare/images/icon2.png" width="180"/> Super Mare 

# Opis igre :memo: :

Super Mare prolazi kroz različite nivoe, izbegava prepreke i sakuplja novcice i zivote. Cilj igre je da sakupi sto vise novcica i time dospe na listu najboljih igraca. 

# Programski jezik
[![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-blue)](https://www.cplusplus.com/) <br>
[![qt6](https://img.shields.io/badge/Framework-Qt6-green)](https://doc.qt.io/qt-6/) <br>

# Okruzenje
[![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-green)](https://www.qt.io/product/development-tools)

# Korisćene biblioteke :books: :
* Qt >= 6.2.2

# Instalacija :hammer: :
**Qt** i **Qt Creator** se mogu preuzeti na [ovom](https://www.qt.io/download) linku. 

# Preuzimanje i pokretanje :wrench: :
1. U terminalu se pozicionirati u zeljeni direktorijum
2. Klonirati repozitorijum komandom: `$ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/13-super-mare.git
3. Otvoriti okruzenje **Qt Creator** i u njemu otvoriti .pro fajl
4. Pritisnuti dugme **Run** (Ctrl+R) u donjem levom uglu ekrana

# Uputstvo :

U glavnom meniju postoje 4 dugmeta:

- Klikom na dugme **Play game** otvara se prozor za prikaz odabira nivoa i unosa imena igraca, kao i dugme **Start** kojim se zapocinje odabrani nivo.
- Klikom na dugme **Rang list** mozete videti tabelu  najboljih rezultata igraca koji su igrali ovu igricu.
- Klikom na dugme **Exit** napustate glavni meni izlazite iz igre.
- Klikom na dugme **Controls** slikovito se prikazuju komande za kretanje igraca.



# Demo snimak igrice :movie_camera: : 

[<img src="SuperMare/images/icon2.png" width="400"/>](https://www.youtube.com/watch?v=o2wep_MJFVM&t=1s)
(klik na sliku vodi na video)

<ul>
    <li><a href="https://gitlab.com/andjelastajic8">Anđela Stajić 147/2018</a></li>
    <li><a href="https://gitlab.com/milican351">Milica Nikolić 89/2018</a></li>
    <li><a href="https://gitlab.com/lidijadjalovic">Lidija Đalović 107/2018</a></li>
    <li><a href="https://gitlab.com/maksimovicj">Jelena Maksimović 193/2018</a></li>
    <li><a href="https://gitlab.com/mich2702">Marko Vićentijević 123/2016</a></li>
</ul>
